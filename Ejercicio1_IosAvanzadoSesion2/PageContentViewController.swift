//
//  PageContentViewController.swift
//  Ejercicio1_IosAvanzadoSesion2
//
//  Created by Fran on 14/11/17.
//  Copyright © 2017 Fran. All rights reserved.
//

import UIKit

class PageContentViewController: UIViewController {
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var pageIndex = 0
    var titleText = ""
    var imageFilename = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
         print("Buscado: \(self.imageFilename)")
        
        self.imageView.image = UIImage(named: self.imageFilename)
        self.titulo.text = self.titleText
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
